package com.crossengage.repository;

import com.crossengage.model.ContactBy;
import com.crossengage.model.User;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserRepositoryTest {

    @Test
    public void testDefault() throws IOException {
        UserRepository repository = new UserRepository(new File(this.getClass().getResource("/test_user_data.txt").getFile()));
        List<User> emails = repository.getAllEmails();
        assertThat(emails).hasSize(6);
        System.out.println(emails);
    }

    @Test
    public void testParse() throws IOException {
        UserRepository repository = new UserRepository(null);
        User user = repository.parse("1,true,email,test1@mail.com,+999999999999");
        assertEquals(1L, user.getId().longValue());
        assertEquals(true, user.getActive());
        assertTrue(user.getContactBy().contains(ContactBy.EMAIL));
        assertEquals(1, user.getContactBy().size());
        assertEquals("test1@mail.com", user.getEmail());
        assertEquals("+999999999999", user.getPhone());
    }

    @Test
    public void testParseContactByEmail() throws IOException {
        UserRepository repository = new UserRepository(null);
        Set<ContactBy> contactBys = repository.getContactBys("email");
        assertTrue(contactBys.contains(ContactBy.EMAIL));
        assertEquals(1, contactBys.size());
    }

    @Test
    public void testParseContactByPhone() throws IOException {
        UserRepository repository = new UserRepository(null);
        Set<ContactBy> contactBys = repository.getContactBys("phone");
        assertTrue(contactBys.contains(ContactBy.PHONE));
        assertEquals(1, contactBys.size());
    }

    @Test
    public void testParseContactByALL() throws IOException {
        UserRepository repository = new UserRepository(null);
        Set<ContactBy> contactBys = repository.getContactBys("all");
        assertTrue(contactBys.contains(ContactBy.PHONE));
        assertTrue(contactBys.contains(ContactBy.EMAIL));
        assertEquals(2, contactBys.size());
    }

    @Test
    public void testParseContactByNONE() throws IOException {
        UserRepository repository = new UserRepository(null);
        Set<ContactBy> contactBys = repository.getContactBys("none");
        assertEquals(0, contactBys.size());
    }

    @Test
    public void testParseContactByNotValidContactType() throws IOException {
        UserRepository repository = new UserRepository(null);
        Set<ContactBy> contactBys = repository.getContactBys("notvalid");
        assertEquals(0, contactBys.size());
    }
}

