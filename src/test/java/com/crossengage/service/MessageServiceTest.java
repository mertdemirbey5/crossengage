package com.crossengage.service;

import com.crossengage.model.ContactBy;
import com.crossengage.model.User;
import name.falgout.jeffrey.testing.junit5.MockitoExtension;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
public class MessageServiceTest {

    @Mock
    private MailService mailService;

    @Mock
    private SMSService SMSService;

    @InjectMocks
    private MessageService messageService;

    private static Set<ContactBy> contactByNone = new HashSet<>();
    private static Set<ContactBy> contactByPhone = new HashSet<>();
    private static Set<ContactBy> contactByEmail = new HashSet<>();
    private static Set<ContactBy> contactByAll = new HashSet<>();

    @BeforeAll
    static void initAll() {
        contactByPhone.add(ContactBy.PHONE);
        contactByEmail.add(ContactBy.EMAIL);
        contactByAll.add(ContactBy.EMAIL);
        contactByAll.add(ContactBy.PHONE);
    }

    @ParameterizedTest
    @MethodSource("validEmailProperties")
    public void testValidEmailUser(Set<ContactBy> contactBySet, Boolean isActive) {
        User user = getUser();
        user.setActive(isActive);
        user.setContactBy(contactBySet);
        String emailContent = "this is a content";
        messageService.sendEmail(user, emailContent);
        verify(mailService).send(emailContent, user.getEmail());
        verify(SMSService, never()).send(any(), any());
    }

    @ParameterizedTest
    @MethodSource("notValidEmailProperties")
    public void testNotSendingEmail(Set<ContactBy> contactBySet, Boolean isActive) {
        User user = getUser();
        user.setActive(isActive);
        user.setContactBy(contactBySet);
        String emailContent = "this is a content";
        messageService.sendEmail(user, emailContent);
        verify(mailService, never()).send(any(), any());
    }

    @ParameterizedTest
    @MethodSource("validSMSProperties")
    public void testValidSMSlUser(Set<ContactBy> contactBySet, Boolean isActive) {
        User user = getUser();
        user.setActive(isActive);
        user.setContactBy(contactBySet);
        String emailContent = "this is a content";
        messageService.sendSMS(user, emailContent);
        verify(SMSService).send(emailContent, user.getPhone());
        verify(mailService, never()).send(any(), any());
    }

    @ParameterizedTest
    @MethodSource("notValidSMSProperties")
    public void testNotValidSMSlUser(Set<ContactBy> contactBySet, Boolean isActive) {
        User user = getUser();
        user.setActive(isActive);
        user.setContactBy(contactBySet);
        String emailContent = "this is a content";
        messageService.sendSMS(user, emailContent);
        verify(SMSService, never()).send(any(), any());
        verify(SMSService, never()).send(any(), any());
    }

    static Stream<Arguments> notValidEmailProperties() {
        return Stream.of(
                Arguments.of(contactByPhone, Boolean.TRUE),
                Arguments.of(contactByNone, Boolean.TRUE),
                Arguments.of(contactByEmail, Boolean.FALSE),
                Arguments.of(contactByAll, Boolean.FALSE)
        );
    }

    static Stream<Arguments> validEmailProperties() {
        return Stream.of(
                Arguments.of(contactByEmail, Boolean.TRUE),
                Arguments.of(contactByAll, Boolean.TRUE)
        );
    }

    static Stream<Arguments> validSMSProperties() {
        return Stream.of(
                Arguments.of(contactByPhone, Boolean.TRUE),
                Arguments.of(contactByAll, Boolean.TRUE)
        );
    }

    static Stream<Arguments> notValidSMSProperties() {
        return Stream.of(
                Arguments.of(contactByEmail, Boolean.TRUE),
                Arguments.of(contactByNone, Boolean.TRUE),
                Arguments.of(contactByPhone, Boolean.FALSE),
                Arguments.of(contactByAll, Boolean.FALSE)
        );
    }

    private User getUser() {
        User user = new User();
        user.setId(1L);
        user.setEmail("test1@mail.com");
        user.setPhone("+999999999996");
        return user;
    }
}
