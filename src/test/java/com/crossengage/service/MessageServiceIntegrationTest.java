package com.crossengage.service;

import com.crossengage.repository.UserRepository;
import name.falgout.jeffrey.testing.junit5.MockitoExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.io.File;
import java.io.IOException;

import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class MessageServiceIntegrationTest {

    @Mock
    private MailService mailService;

    @Mock
    private SMSService SMSService;

    @InjectMocks
    private MessageService messageService = new MessageService(mailService, SMSService,
            new UserRepository(new File(this.getClass().getResource("/test_user_data.txt").getFile())));


    @Test
    public void testSendingTestUserDataFile() throws IOException {
        messageService.run();
        //user 1
        verify(mailService).send("Welcome to our system", "test1@mail.com");
        verify(SMSService, never()).send("Welcome to our system", "+999999999999");

        //user 2
        verify(mailService, never()).send("Welcome to our system", "test2@mail.com");
        verify(SMSService, never()).send("Welcome to our system", "+999999999998");

        //user 3
        verify(mailService, never()).send("Welcome to our system", "test3mail.com");
        verify(SMSService, never()).send("Welcome to our system", "+999999999997");

        //user 4 & 5
        //that is actuall a serious bug we should not send duplicate message to user.
        verify(mailService, times(2)).send("Welcome to our system", "test4@mail.com");
        verify(SMSService).send("Welcome to our system", "+999999999996");

        //user 6
        verify(SMSService).send("Welcome to our system", "+555555555555");
        verify(mailService).send("Welcome to our system", "test5@mail.com");
    }
}
