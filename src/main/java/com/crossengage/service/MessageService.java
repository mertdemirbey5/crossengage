package com.crossengage.service;

import com.crossengage.MessageSender;
import com.crossengage.model.ContactBy;
import com.crossengage.model.User;
import com.crossengage.repository.UserRepository;

import java.io.IOException;

public class MessageService {
    private static final String messageContent = "Welcome to our system";
    //Below services should be injected with dependency injection
    private MailService mailService = new MailService();
    private SMSService SMSService = new SMSService();
    private UserRepository userRepository;

    public MessageService(MailService mailService, com.crossengage.service.SMSService SMSService, UserRepository userRepository) {
        this.mailService = mailService;
        this.SMSService = SMSService;
        this.userRepository = userRepository;
    }

    public void run() throws IOException {
        MessageSender sender = new MessageSender();
        sender.addConsumer(this::sendEmail);
        sender.addConsumer(this::sendSMS);
        userRepository.getAllEmails().forEach( u -> sender.getConsumer().accept(u, messageContent));
    }

    void sendEmail(User user, String message){
        if(Boolean.TRUE.equals(user.getActive() && user.getContactBy().contains(ContactBy.EMAIL))) {
            mailService.send(message, user.getEmail());
        }
    }

    void sendSMS(User user, String message) {
        if (Boolean.TRUE.equals(user.getActive() && user.getContactBy().contains(ContactBy.PHONE))) {
            SMSService.send(message, user.getPhone());
        }
    }
}
