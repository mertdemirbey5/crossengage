package com.crossengage.model;

import java.util.HashSet;
import java.util.Set;

public class User {
    private Long id;
    private Boolean active;
    private Set<ContactBy> contactBy = new HashSet<>();
    private String email;
    private String phone;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Set<ContactBy> getContactBy() {
        return contactBy;
    }

    public void setContactBy(Set<ContactBy> contactBy) {
        this.contactBy = contactBy;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", active=" + active +
                ", contactBy=" + contactBy +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
