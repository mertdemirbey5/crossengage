package com.crossengage;

import com.crossengage.repository.UserRepository;
import com.crossengage.service.MailService;
import com.crossengage.service.MessageService;
import com.crossengage.service.SMSService;

import java.io.File;
import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException {
        MessageService messageService = new MessageService(new MailService(), new SMSService(),
                new UserRepository(new File(args[0])));
        messageService.run();
    }
}
