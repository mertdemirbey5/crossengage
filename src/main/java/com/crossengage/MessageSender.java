package com.crossengage;

import com.crossengage.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

public class MessageSender {
    List<BiConsumer<User, String>> consumers = new ArrayList<>();

    public void addConsumer(BiConsumer<User, String> consumer) {
        consumers.add(consumer);
    }

    public BiConsumer getConsumer() {
        return consumers.stream().reduce((c1, c2) -> c1.andThen(c2)).get();
    }
}
