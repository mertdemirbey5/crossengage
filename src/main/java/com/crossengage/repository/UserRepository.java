package com.crossengage.repository;

import com.crossengage.model.ContactBy;
import com.crossengage.model.User;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserRepository {

    private File data;
    private static Logger logger = Logger.getLogger( UserRepository.class.getName() );

    public UserRepository(File data) {
        this.data = data;
    }

    public List<User> getAllEmails() throws IOException {
        try (Stream<String> lines = Files.lines(data.toPath())) {
            return lines.skip(1)
                    .map(line -> parse(line))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
    }

    User parse(String userString){
        String[] userInfo =  userString.split(",");
        User user = null;
        if(userInfo.length == 5){
            user = new User();
            user.setId(Long.parseLong(userInfo[0]));
            user.setActive(Boolean.parseBoolean(userInfo[1]));
            String contactString = userInfo[2];
            user.setContactBy(getContactBys(contactString));
            user.setEmail(userInfo[3]);
            user.setPhone(userInfo[4]);
        }else{
            logger.warning("User information can't be parsed user = "+ Arrays.toString(userInfo));
        }
        return user;
    }

    Set<ContactBy> getContactBys(String contactString){
        Set<ContactBy> contactBy = new HashSet<>();
        if("email".equals(contactString)){
            contactBy.add(ContactBy.EMAIL);
        }else if("phone".equals(contactString)){
            contactBy.add(ContactBy.PHONE);
        }else if("all".equals(contactString)){
            contactBy.add(ContactBy.EMAIL);
            contactBy.add(ContactBy.PHONE);
        }
        return contactBy;
    }
}
